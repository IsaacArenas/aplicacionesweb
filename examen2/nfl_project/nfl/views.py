from django.http import HttpResponse
from django.shortcuts import render
from .models import Ciudad, Equipo

def home(request):
    return render(request, 'home.html')
def lista_equipos(request):
    equipos = Equipo.objects.all()
    return render(request, 'nfl/lista_equipos.html', {'equipos': equipos})

def lista_ciudades(request):
    ciudades = Ciudad.objects.all()
    return render(request, 'nfl/lista_ciudades.html', {'ciudades': ciudades})
